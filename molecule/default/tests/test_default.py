import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_borgbackup(host):
    ps = host.run("/usr/local/borgbackup/bin/borgbackup_runner.sh borgbackup")
    assert ps.rc == 0
